#include <iostream>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <queue>
#include <algorithm>
#include <stdlib.h>
#include <string.h>
#define MAXN 10010
#define INF 100000000
using namespace std;

int N;
int pd[MAXN];
int vis[MAXN];
vector< vector<int> > v;
int dfs(int n){
    vis[n] = 1;
    int ans = 0;
    for(int i =0;i<v[n].size();i++){
        if(vis[v[n][i]] == -1)
            ans+=1+dfs(v[n][i]);
    }
   return ans;

}
int main(){
    //freopen("in.txt","r",stdin);
    int N,K;
    for(;cin>> N;){
        if(N == 0)
            break;
        v.erase(v.begin(),v.end());
        memset(pd,-1,sizeof(pd));
        for(int i=0;i<N;i++){
            cin >> K;
            vector<int> ans;
            v.push_back(ans);
            for(int j=0;j<K;j++){
                int w;
                cin >> w;
                v[i].push_back(w-1);
            }
        }

        int ans = 0;
        int idx = 0;
        for(int i=0;i<N;i++){
            memset(vis,-1,sizeof(vis));
            int k = dfs(i);
            if( k > ans){
                ans = k;
                idx = i;
            }
        }
        cout << idx+1 << endl;
    }
}

