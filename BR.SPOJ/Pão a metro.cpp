#include <iostream>
#include <vector>
#include <cmath>
#include <stdio.h>

using namespace std;

int bs(int s,int ma,int n,vector<int> a){

    if( s < n)
        return 0;

    int ans = 0;
    int l = 0;
    int h = ma;

    while(l <= h){

        int m = floor((l + h)/2);
        if(m == 0) m++;
        if(m*n <= s){
            int q = 0;
            for(int i=0;i<a.size();i++){
                q+= floor(a[i]/m);
                if( q >= n) break;
            }
            if( q >= n){
                ans = m;
                l = m+1;
            }else
                h = m-1;
        }else
            h = m-1;
    }
    return ans;
}
int main(){
    int N,P;
    int flag = 0;
    while(cin >> N >> P){
        if(flag)  cout << endl;
        vector<int> v;
        int MA = 0;
        int S = 0;
        for(int i=0;i<P;i++){
            int x;
            cin >> x;
            v.push_back(x);
            S+=x;
            MA = max(MA,x);
        }
        cout << bs(S,MA,N,v);
        flag ++;
    }


}
