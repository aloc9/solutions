#include <iostream>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <queue>
#define MAXN 15
#define INF 100000000
using namespace std;

int N,M;
int a[MAXN][MAXN];
int v[MAXN][MAXN];
int idx[] = {-1,0,1,0};
int idy[] = {0,1,0,-1};

int main(){
    int flag = 0;
    cin >> N >> M;

        pair<pair<int,int>,int> k;
        for(int i=0;i<N;i++){
            for(int j=0;j<M;j++){
                cin >> a[i][j];
                if(a[i][j] == 3){
                    k = make_pair(make_pair(i,j),0);
                }
                v[i][j] = 0;
            }
        }

        queue<pair<pair<int,int>,int> >q;
        q.push(k);
        int b = INF;

        while(!q.empty()){
            int i = (q.front().first).first;
            int j = (q.front().first).second;
            int c = q.front().second;
            q.pop();
            if(a[i][j] == 0){
                b = min(b,c);
                continue;
            }else
            v[i][j] = 1;


            for(int k=0;k<4;k++){
                int ni = i+idx[k];
                int nj = j+idy[k];
                if(ni>=0 && nj>= 0 && ni<N && nj<M && v[ni][nj] == 0 && a[ni][nj] != 2){
                    q.push(make_pair(make_pair(ni,nj),c+1));
                }
            }
        }
        cout << b;

}

