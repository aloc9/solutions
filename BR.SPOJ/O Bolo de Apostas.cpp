#include <iostream>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <queue>
#define MAXN 10010
#define INF 100000000
using namespace std;

int N,b;
int a[MAXN];
int pd[MAXN];
int main(){
    //freopen("in.txt","r",stdin);

    while(true){
        scanf("%d",&N);
        if( N == 0 ) break;
        for(int i=1;i<=N;i++){
            scanf("%d",&a[i]);
        }
        pd[0] = 0;
        b = 0;
        for(int k=1;k<=N;k++){
            pd[k] = max(pd[k-1]+a[k],a[k]);
            b = max(b,pd[k]);
        }

    if(b>0)
        printf("The maximum winning streak is %d.\n",b);
    else
        cout << "Losing streak.\n";
    }
}

