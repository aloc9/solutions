#include <iostream>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <queue>
#include <algorithm>
#include <stdlib.h>
#include <string.h>
#define MAXN 10010
#define INF 100000000
using namespace std;

int N;
vector<int> v[MAXN];
int vis[MAXN];
int pd[MAXN][2];

int dfs(int n,int c){
    if(pd[n][c] != -1){
        return pd[n][c];
    }

    int ans = 0;
    vis[n] = 1;
    for(int j=0;j<v[n].size();j++){
        if(vis[v[n][j]] == -1){
            if( c == 0){
                int ans2 = 1+dfs(v[n][j],1);
                int ans1 = dfs(v[n][j],0);
                ans+= max(ans1,ans2);
            }else
                ans+= dfs(v[n][j],0);
        }
    }
    vis[n] = -1;
    pd[n][c] = ans;
    return ans;
}
int main(){
    //freopen("in.txt","r",stdin);
    int T;
    scanf("%d",&T);
    for(int i=0;i<T;i++){
        scanf("%d",&N);
        for(int k=0;k<N;k++){
            vis[k] = -1;
            pd[k][0] = -1;
            pd[k][1] = -1;
            vector<int> ans;
            v[k] = ans;
        }
        for(int j=0;j<N-1;j++){
            int s,t;
            scanf("%d %d",&s,&t);
            v[s-1].push_back(t-1);
            v[t-1].push_back(s-1);
        }
        int ans =  max(dfs(0,0),1+dfs(0,1));
        cout << ans;
        if(i != T-1)
            cout << endl;
    }
}

